package com.rj.seckill.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rj.seckill.entity.SecKill;

@RunWith(SpringJUnit4ClassRunner.class)
//告诉junit spring配置文件的位置
@ContextConfiguration("classpath:spring/spring-dao.xml")
public class TsetSecKillDAO {

	@Autowired
	private SecKillDAO secKillDAO;
	
	@Test
	public void textSecKillList(){
		int limit = 10;
		int offset = 0;
		List<SecKill> secKills = secKillDAO.queryAll(limit, offset);
		System.out.println(secKills.size());
	}
	
	@Test
	public void testSecKillById (){
		long secKillId = 2;
		SecKill secKill = secKillDAO.getById(secKillId);
		System.out.println(secKill);
	}  
}
