package com.rj.seckill.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rj.seckill.entity.SecKill;
import com.rj.seckill.entity.SuccessKilled;

@RunWith(SpringJUnit4ClassRunner.class)
//告诉junit spring配置文件的位置
@ContextConfiguration("classpath:spring/spring-dao.xml")
public class TsetSuccessKilledDAO {

	@Autowired
	private SecKillDAO secKillDAO;
	@Autowired
	private SuccessKilledDAO successKilledDAO;
	
	
	@Test
	public void textSuccessKillList(){
		int secKillId = 1;
		String userPhome = "18513108040";
		SuccessKilled successKilled = successKilledDAO.queryByIdWithSecKill(secKillId, userPhome);
		System.out.println(successKilled);
	}
	
	@Test
	public void testSaveSuccessKilled(){
		long secKillId = 1;
		String userPhone = "18513108040";
		int save = successKilledDAO.save(secKillId, userPhone);
		System.out.println(save);
	}  
}
