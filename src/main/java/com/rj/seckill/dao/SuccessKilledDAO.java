package com.rj.seckill.dao;

import org.apache.ibatis.annotations.Param;

import com.rj.seckill.entity.SuccessKilled;

public interface SuccessKilledDAO {
	
	/**
	 * 秒杀
	 */
	int save (@Param("secKillId") long secKillId,@Param("userPhone") String userPhone);
	
	/**
	 * 查询秒杀成功列表
	 */
	SuccessKilled queryByIdWithSecKill(@Param("secKillId") long secKillId,
			@Param("userPhone") String userPhone);
	
}
