package com.rj.seckill.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.rj.seckill.entity.SecKill;

public interface SecKillDAO {

	SecKill getById(long secKillId);
	
	List<SecKill> queryAll(@Param("limit") int limit,@Param("offset") int offset);
	
	int update(@Param("secKillId") long secKillId ,@Param("startTime") Date startTime);
	
	
	void killByProcedure(Map<String,Object> paramMap);
}
