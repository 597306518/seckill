package com.rj.seckill.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rj.seckill.dto.Exposer;
import com.rj.seckill.dto.SecKillExecution;
import com.rj.seckill.dto.SeckillResult;
import com.rj.seckill.entity.SecKill;
import com.rj.seckill.enums.SeckillStatusEnum;
import com.rj.seckill.exception.RepeatKillException;
import com.rj.seckill.exception.SeckillCloseException;
import com.rj.seckill.service.SuccessKilledService;

@Controller
@RequestMapping("/seckill")  //module
public class SeckillController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private SuccessKilledService seckillService;

	/**
	 * get all seckill list
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public String list(Model model){
		//list.jsp + model = ModelAndView
		List<SecKill> seckills = seckillService.getSecKillList();
		model.addAttribute("seckills", seckills);
		return "list";  //WEB-INF/jsp/list.jsp    look at spring-web.xml
	}

	/**
	 * get seckill by seckillId
	 * @param seckillId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{seckillId}/detail",method = RequestMethod.GET)
	public String detail(@PathVariable("seckillId") Long seckillId,Model model){
		if(seckillId == null){
			return "redirect:/seckill/list";
		}
		SecKill seckill = seckillService.getSecKill(seckillId);
		if(null == seckill){
			return "forward:/seckill/list";
		}
		model.addAttribute("seckill",seckill);
		return "detail";
	}

	//ajax return json
	@RequestMapping(value = "/{seckillId}/exposer",method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8"})//http context
	@ResponseBody  //packging return result to json
	public SeckillResult<Exposer> exposer(@PathVariable Long seckillId){
		SeckillResult<Exposer> result;
		try{
			Exposer exposer = seckillService.exportSecKillUrl(seckillId);
			result = new SeckillResult<Exposer>(true,exposer);
		}catch (Exception e){
			logger.error(e.getMessage());
			result = new SeckillResult<Exposer>(false,e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/{seckillId}/{md5}/execution",method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public SeckillResult<SecKillExecution> execute(@PathVariable("seckillId") Long seckillId,@PathVariable("md5") String md5,
			@CookieValue(value = "killPhone",required = false) String phone){
		if(phone == null){
			return new SeckillResult<SecKillExecution>(false,"do not login");
		}
		try{
			SecKillExecution seckillExcution = seckillService.execurteSecKill(seckillId, phone, md5);
			return new SeckillResult<SecKillExecution>(true,seckillExcution);
		}catch (RepeatKillException e){
			SecKillExecution seckillExcution = new SecKillExecution(seckillId, SeckillStatusEnum.REPEAT_KILL);
			return new SeckillResult<SecKillExecution>(true,seckillExcution);
		}catch (SeckillCloseException e){
			SecKillExecution seckillExcution = new SecKillExecution(seckillId,SeckillStatusEnum.END);
			return new SeckillResult<SecKillExecution>(true,seckillExcution);
		}
		catch (Exception e){
			logger.error(e.getMessage());
			SecKillExecution seckillExcution = new SecKillExecution(seckillId,SeckillStatusEnum.INNER_ERROR);
			return new SeckillResult<SecKillExecution>(true,seckillExcution);
		}
	}

	@RequestMapping(value = "/time/now",method = RequestMethod.GET,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public SeckillResult<Long> time(){
		Date date = new Date();
		return  new SeckillResult<Long>(true,date.getTime());
	}
}