package com.rj.seckill.dto;

import com.rj.seckill.entity.SuccessKilled;
import com.rj.seckill.enums.SeckillStatusEnum;

public class SecKillExecution {

	/**
	 * 秒杀id
	 */
	private long secKIllId;
	/**
	 * 执行秒杀状态
	 * 编码，描述
	 */
	private SeckillStatusEnum seckillStatusEnum;
	
	/**
	 * 秒杀成功后返回秒杀结果
	 */
	private SuccessKilled successKilled;

	public SecKillExecution() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SecKillExecution(long secKIllId, SeckillStatusEnum seckillStatusEnum, SuccessKilled successKilled) {
		super();
		this.secKIllId = secKIllId;
		this.seckillStatusEnum = seckillStatusEnum;
		this.successKilled = successKilled;
	}
	
	

	public SecKillExecution(long secKIllId, SeckillStatusEnum seckillStatusEnum) {
		super();
		this.secKIllId = secKIllId;
		this.seckillStatusEnum = seckillStatusEnum;
	}

	public long getSecKIllId() {
		return secKIllId;
	}

	public void setSecKIllId(long secKIllId) {
		this.secKIllId = secKIllId;
	}

	public SeckillStatusEnum getSeckillStatusEnum() {
		return seckillStatusEnum;
	}

	public void setSeckillStatusEnum(SeckillStatusEnum seckillStatusEnum) {
		this.seckillStatusEnum = seckillStatusEnum;
	}

	public SuccessKilled getSuccessKilled() {
		return successKilled;
	}

	public void setSuccessKilled(SuccessKilled successKilled) {
		this.successKilled = successKilled;
	}
	
	
}
