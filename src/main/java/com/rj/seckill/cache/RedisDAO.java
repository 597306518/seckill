package com.rj.seckill.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dyuproject.protostuff.LinkedBuffer;
import com.dyuproject.protostuff.ProtostuffIOUtil;
import com.dyuproject.protostuff.runtime.RuntimeSchema;
import com.rj.seckill.entity.SecKill;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisDAO {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final JedisPool jedisPool;
	
	public RedisDAO (String ip , int port){
		jedisPool = new JedisPool(ip , port);
	}
	
	private RuntimeSchema<SecKill> schema = RuntimeSchema.createFrom(SecKill.class);
	
	public SecKill getSecKill(long secKillId){
		try {
			Jedis jedis = jedisPool.getResource();
			try {
				String key = "secKill:" + secKillId;
				//java 并没有实现内部序列化
				//第三方序列化工具
				// get方法-->byte[] -->反序列化 --> Object(SecKill)
				//protosuff :pojo
				byte[] bytes = jedis.get(key.getBytes());
				//从缓存中获取
				if (bytes != null) {
					//空对象
					SecKill secKill = schema.newMessage();
					ProtostuffIOUtil.mergeFrom(bytes, secKill, schema);
					//secKIll被反序列化
					return secKill;
				}
			}finally {
				jedis.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		return null;
	}
	
	public String putSecKill(SecKill secKill){
		// set Object(SecKill) -->序列化 -->byte[]
		try {
			Jedis jedis = jedisPool.getResource();
			try {
				String key = "secKill:" + secKill.getSeckillId();
				byte[] bytes = ProtostuffIOUtil.toByteArray(secKill, schema, 
						LinkedBuffer.allocate(LinkedBuffer.DEFAULT_BUFFER_SIZE));
				int timeout = 60 * 60; //缓存 1小时
				String result = jedis.setex(key.getBytes(), timeout, bytes);
				return result;
			}finally {
				jedis.close();
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return null;
	}
}
