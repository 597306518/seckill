package com.rj.seckill.service;


import java.util.List;

import com.rj.seckill.dto.Exposer;
import com.rj.seckill.dto.SecKillExecution;
import com.rj.seckill.entity.SecKill;
import com.rj.seckill.exception.RepeatKillException;
import com.rj.seckill.exception.SeckillCloseException;
import com.rj.seckill.exception.SeckillException;

public interface SuccessKilledService {
	
	/**
	 * 所有秒杀商品列表
	 */
	
	List<SecKill> getSecKillList();

	/**
	 * 查询单个秒杀商品
	 * @param secKillId
	 * @return
	 */
	SecKill getSecKill(long secKillId);
	
	/**
	 * 查询秒杀是否开启
	 * 若没有开启，返回开启秒杀的剩余时间
	 * 若以开启，返回秒杀地址
	 */
	Exposer exportSecKillUrl(long secKillId);
	
	
	/**
	 * 执行秒杀
	 * 
	 * 秒杀成功返回，秒杀的商品
	 * 秒杀失败，返回失败的提示信息
	 */
	SecKillExecution execurteSecKill(long secKillId,String userPhone,String md5)
	throws RepeatKillException,SeckillCloseException,SeckillException;
	
	
	SecKillExecution executeSeckillProcedure(long seckillId,String userPhone,String md5) 
			throws SeckillException,RepeatKillException,SeckillCloseException;
}
