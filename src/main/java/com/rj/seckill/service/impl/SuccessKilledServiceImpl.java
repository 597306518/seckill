package com.rj.seckill.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.rj.seckill.cache.RedisDAO;
import com.rj.seckill.dao.SecKillDAO;
import com.rj.seckill.dao.SuccessKilledDAO;
import com.rj.seckill.dto.Exposer;
import com.rj.seckill.dto.SecKillExecution;
import com.rj.seckill.entity.SecKill;
import com.rj.seckill.entity.SuccessKilled;
import com.rj.seckill.enums.SeckillStatusEnum;
import com.rj.seckill.exception.RepeatKillException;
import com.rj.seckill.exception.SeckillCloseException;
import com.rj.seckill.exception.SeckillException;
import com.rj.seckill.service.SuccessKilledService;
import com.rj.seckill.util.MD5Util;
@Service
public class SuccessKilledServiceImpl implements SuccessKilledService {
 
	@Autowired
	private SecKillDAO secKillDAO;
	@Autowired
	private SuccessKilledDAO successKilledDAO;
	@Autowired
	private RedisDAO redisDAO;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public List<SecKill> getSecKillList() {
		List<SecKill> result = null;
		int limit = 10;
		int offset = 0;
		result = secKillDAO.queryAll(limit, offset);
		return result;
	}

	@Override
	public SecKill getSecKill(long secKillId) {
		SecKill result = null;
		result = secKillDAO.getById(secKillId);
		return result;
	}

	@Override
	public Exposer exportSecKillUrl(long secKillId) {
		/**
		 * 优化点
		 */
		//1.访问redis
		SecKill secKill = redisDAO.getSecKill(secKillId);
		if (secKill == null) {
			//2.访问数据库
			secKill = getSecKill(secKillId);
			if (secKill == null) {
				return new Exposer(false ,secKillId);
			}else{
				//3.放入redis中
				redisDAO.putSecKill(secKill);
			}
		}
		
		long endTime = secKill.getEndTime().getTime();
		long startTime = secKill.getStartTime().getTime();
		long nowTime = new Date().getTime();
		if (nowTime < startTime || nowTime > endTime) {
			return new Exposer(false,secKillId,nowTime,startTime,endTime);
		}
		
		String md5 = MD5Util.getMD5(String.valueOf(secKillId));
		return new Exposer(true,md5,secKillId);
	}

	@Override
	@Transactional
	/**
	 * 1.保证事物方法的执行时间尽可能短，不要穿插其他的网络操作RPC/HTTP
	 * 	或者剥离到事物方法外边
	 * 2.不是所有的方法都需要事务，只有一条添加，修改，只读操作不需要事物
	 * 
	 */
	public SecKillExecution execurteSecKill(long secKillId, String userPhone, String md5)
			throws RepeatKillException, SeckillCloseException, SeckillException {
		String md5DB = MD5Util.getMD5(String.valueOf(secKillId));
		if (!StringUtils.hasText(md5) || !md5.equals(md5DB)) {
			throw new SeckillException("seckill data rewrite");
		}
		Date nowTime = new Date();
		try{
			//保存秒杀数据
			int save = successKilledDAO.save(secKillId, userPhone);
			if (save <= 0) {
				//重复秒杀
				throw new RepeatKillException("seckill repeated");
			}else{
				//减库存 ,热点商品竞争
				int update = secKillDAO.update(secKillId, nowTime);
				if (update <= 0) {
					//没有更新菲奥记录，秒杀结束
					throw new SeckillCloseException("seckill is close");
				}else{
					//秒杀成功
					SuccessKilled successKilled = successKilledDAO.queryByIdWithSecKill(secKillId, userPhone);
					return new SecKillExecution(secKillId,SeckillStatusEnum.SUCCESS,successKilled);
				}
			}
		}catch (RepeatKillException e1) {
			throw e1;
		}catch (SeckillCloseException e2) {
			throw e2;
		}catch (SeckillException e){
			logger.error(e.getMessage());
			throw new SeckillException("seckill inner error:"+e.getMessage());
		}
	
	}
	
	
	
	public SecKillExecution executeSeckillProcedure(long secKillId, String userPhone, String md5)
			throws SeckillException, RepeatKillException, SeckillCloseException {
		String md5DB = MD5Util.getMD5(String.valueOf(secKillId));
		if (!StringUtils.hasText(md5) || !md5.equals(md5DB)) {
			throw new SeckillException("seckill data rewrite");
		}
		Date time = new Date();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("seckillId",secKillId);
		map.put("phone",userPhone);
		map.put("killTime",time);
		map.put("result",null);
		try {
			secKillDAO.killByProcedure(map);
			int result = MapUtils.getInteger(map,"result",-2);
			if(result == 1){
				SuccessKilled successKill = successKilledDAO.queryByIdWithSecKill(secKillId,userPhone);
				return new SecKillExecution(secKillId,SeckillStatusEnum.SUCCESS,successKill);
			}
			else{
				return new SecKillExecution(secKillId,SeckillStatusEnum.statusOf(result));
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			return new SecKillExecution(secKillId,SeckillStatusEnum.INNER_ERROR);
		}
	}
}
