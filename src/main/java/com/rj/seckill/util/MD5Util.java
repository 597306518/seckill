package com.rj.seckill.util;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

public class MD5Util {

	public static String getMD5(String value) throws RuntimeException{
		String result = "";
		if (StringUtils.hasText(value)) {
			try {
				String base = value + "/" + RandomSaltUtils.getRandomSalt(128);
				result = DigestUtils.md5DigestAsHex(base.getBytes());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
		
	}
}
