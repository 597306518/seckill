package com.rj.seckill.util;

import java.util.Random;

public class RandomSaltUtils {

	public static String getRandomSalt(int n){
		char[] c = {'A','B','C','D','1','2','3','4','5','6','7','8','9','0','E','F','G','H','I','G','K','L','M','N','O','P'
				,'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','q','w','e','r',';',':','/','?'
				,'b','v','c','x','z','a','s','d','f','g','h','j','k','l','p','o','i','u','y','t',',','<'
				,'*','&','^','%','$','#','!','~','`','z','x','(',')','[',']','{','}','\\','|','.','>'
		};
		Random random = new Random();
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < n; i++) {
			stringBuilder.append(c[random.nextInt(c.length)]);
		}
		return stringBuilder.toString();
	}
	
	
	public static void main(String[] args) {
		System.out.println(getRandomSalt(128));
	}
}
