package com.rj.seckill.entity;

import java.util.Date;

public class SuccessKilled {

	private long seckillId;

	private String userPhone;

	private short status;

	private Date createTime;
		
	//many to one
	private SecKill secKill;

	public SuccessKilled() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SuccessKilled(long seckillId, String userPhone, short status, Date createTime, SecKill secKill) {
		super();
		this.seckillId = seckillId;
		this.userPhone = userPhone;
		this.status = status;
		this.createTime = createTime;
		this.secKill = secKill;
	}

	public long getSeckillId() {
		return seckillId;
	}

	public void setSeckillId(long seckillId) {
		this.seckillId = seckillId;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public SecKill getSecKill() {
		return secKill;
	}

	public void setSecKill(SecKill secKill) {
		this.secKill = secKill;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	@Override
	public String toString() {
		return "SuccessKilled [seckillId=" + seckillId + ", userPhone=" + userPhone + ", status=" + status
				+ ", createTime=" + createTime + ", secKill=" + secKill + "]";
	}

	
	
}
