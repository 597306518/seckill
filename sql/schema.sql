-- database init
--create secound kill table
CREATE TABLE seckill(
	seckill_id bigserial primary key not null ,
	name varchar(50) not null,
	number smallint not null,
	start_time timestamp not null ,
	end_time timestamp not null,
	create_time timestamp not null default current_timestamp
);

create index index_start_time on seckill (start_time);
create index index_end_time on seckill (end_time);


--init data
INSERT INTO seckill (name,number,start_time,end_time)
VALUES
  ('888 yuan secound kill',888,'2016-12-16 00:00:00','2016-12-30 00:00:00'),
  ('777 yuan secound kill',777,'2016-12-16 00:00:00','2016-12-30 00:00:00'),
  ('666 yuan secound kill',666,'2016-12-16 00:00:00','2016-12-30 00:00:00');
  
  

--secound kill detail
--user login information
CREATE TABLE successed_kill(
	seckill_id bigint not null,
	user_phone varchar(15) not null,
	status smallint not null default -1,
	create_time timestamp not null default current_timestamp ,
	PRIMARY KEY (seckill_id,user_phone)
);

create index index_create_time on successed_kill (create_time);

--connect mysql console
mysql -uroot -p