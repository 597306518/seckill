create table rj_user_info(
	user_id, 
	user_name,
	user_code,
	user_pwd,
	user_base_id,
	name,
	gender,
	age,
	birth_day,
	email,
	identify_phone,
	indenity_email,
	user_type,  --用户类型
	user_status,	--用户状态
	is_public,	--是否公开  1：公开，可以登陆前后台  ，0不公开，只能登陆前台
	is_manager, --是否是管理员
	qq,
	msn,
	line,
	webchat，   --微信
	alipay，--支付宝
	creat_user,
	create_time,
	create_ea_id,
	up_user,
	up_time,
	is_del,
	del_user,
	del_time

);



create table rj_base_user_info(
	user_info_id,
	reg_source,  --数据来源
	user_reg_time, --用户注册时间
	user_log_time, --用户登陆时间
	user_log_ip, --登陆ip，
	reg_ip ,  --注册ip
	province_code, --省
	city_code ,--市
	areat_code, --区县
	street_code, --街道
	address,
	post_code, --邮编
	tel, --电话
	mobile,  --手机
	nation_code,--民族
	occupation,--职业
	ed_background,--教育北京
	marital_status,--婚姻
	cert_type,--证件类型，1.身份证，2.军官证，3.护照
	cert_no,--证件号码
	photo_small,
	photo_middle,
	photo_big,
	registered_type,--户籍类型
	registered_area_id,--户籍注册地
	registered_area_address，--户籍地s址
	contcat_name,--联系人
	contcat_mobile,--联系人电话
	contcat_email,--联系人邮件
	registered_province_code, --户籍省
	registered_city_code ,--户籍市
	registered_areat_code, --户籍区县
	registered_street_code, --户籍街道
	interests,--兴趣爱好
	r_org_name,--工作单位
	remark，--说明
	org_id --工作单位i外键
);

create table rj_org_info(
	org_id,
	org_code,
	org_name,
	org_simple_name,
	org_address,
	province_code, --省
	city_code ,--市
	areat_code, --区县
	street_code, --街道
	org_tel,--电话
	org_fax,--传真
	org_website,--网站
	org_post_code,--邮编
	org_grade, --级别
	org_class,--分类
	org_parent_id,--上级公司
	org_type,--单位类型
	concate_name,--联系人
	concate_gender,
	concate_departments,--联系人部门
	concate_position,--联系人职务
	concate_eamil,--邮件
	remark--说明
);
